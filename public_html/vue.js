'use strict';

let router ;
let app ;
let wd = new WikiData() ;

let subjects = {} ;

$(document).ready ( function () {


    vue_components.toolname = 'new-page-linker' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
        vue_components.loadComponents ( ['wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','autodesc','typeahead-search','value-validator',
            'vue_components/main-page.html',
            ] )
    ] )
    .then ( () => {

            wd_link_wd = wd ;
          const routes = [
            { path: '/', component: MainPage , props:true },
            { path: '/:initial_language', component: MainPage , props:true },
            { path: '/:initial_language/:initial_number_of_recent_changes', component: MainPage , props:true },
          ] ;
          router = new VueRouter({routes}) ;
          app = new Vue ( { router } ) .$mount('#app') ;

    } ) ;
} ) ;
